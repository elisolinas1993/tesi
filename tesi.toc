\babel@toc {italian}{}
\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {chapter}{\numberline {1}Introduzione}{3}% 
\contentsline {subsubsection}{Esempio 1}{3}% 
\contentsline {section}{\numberline {1.1}File \texttt {zshortage}}{5}% 
\contentsline {section}{\numberline {1.2}BOM (Bill of Material)}{6}% 
\contentsline {section}{\numberline {1.3}Note sulla versione del progetto presentata in questa relazione}{6}% 
\contentsline {chapter}{\numberline {2}Parsing del file BOM e costruzione degli alberi}{7}% 
\contentsline {section}{\numberline {2.1}Strutture dati per la rappresentazione degli \texttt {Item}}{7}% 
\contentsline {section}{\numberline {2.2}Parsing dell'input e costruzione della foresta BOM}{8}% 
\contentsline {chapter}{\numberline {3}Algoritmo di ricerca in una foresta}{10}% 
\contentsline {subsubsection}{Esempio 2}{10}% 
\contentsline {section}{\numberline {3.1}Rappresentazione dei percorsi nell'albero BOM}{11}% 
\contentsline {section}{\numberline {3.2}Ricerca dell'enditem all'interno della foresta}{12}% 
\contentsline {section}{\numberline {3.3}Ricerca dei percorsi all'interno di un albero}{12}% 
\contentsline {chapter}{\numberline {4}GoLang}{15}% 
