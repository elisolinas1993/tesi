package bom

import (
	"errors"
)

// Lookup returns a list of all paths that include the given path.
func (bom BOM) Lookup(incomplete []string) (*PathList, error) {
	enditem := &Item{}
	if _, exists := bom[incomplete[0]]; exists {
		enditem = bom[incomplete[0]]
	} else {
		for _, enditems := range bom {
			enditem = enditems.Find(incomplete[0])
		}
	}
	if enditem == nil {
		return nil, errors.New("There is no enditem with ID: " + incomplete[0])
	}

	pathList := &PathList{
		Paths:      []Path{},
		TotalValue: 0,
	}
	thisPath := Path{
		Root:  enditem,
		Items: []*Child{},
		Value: 1,
	}

	enditem.lookup(incomplete, thisPath, pathList)

	return pathList, nil
}

func (enditem *Item) lookup(incomplete []string, thisPath Path, pathList *PathList) {
	for _, child := range enditem.Children {
		child.lookup(incomplete[1:], thisPath, pathList)
	}
}

func (item *Child) lookup(incomplete []string, thisPath Path, pathList *PathList) {
	if len(incomplete) == 0 {
		return
	}
	thisPath.Items = append(thisPath.Items, item)
	thisPath.Value *= item.Quantity

	if incomplete[0] == item.Element.ID {
		if len(incomplete) == 1 {
			pathList.Paths = append(pathList.Paths, thisPath)
			pathList.TotalValue += thisPath.Value
		}
		incomplete = incomplete[1:]
	}
	for _, child := range item.Element.Children {
		child.lookup(incomplete, thisPath, pathList)
	}
}

// Find returns the item with the ID specified in given string
func (item *Item) Find(wanted string) *Item {
	q := []*Item{item}
	for len(q) > 0 {
		i := q[0]
		q = q[1:]
		if i.ID == wanted {
			return i
		}
		for _, child := range i.Children {
			q = append(q, child.Element)
		}
	}
	return nil
}
