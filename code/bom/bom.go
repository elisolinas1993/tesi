package bom

import (
	"strconv"
	"strings"
)

type Item struct {
	ID       string
	Children []*Child
}

type Child struct {
	Element  *Item
	Quantity int
}

type BOM map[string]*Item

type Path struct {
	Root  *Item
	Items []*Child
	Value int
}

type PathList struct {
	Paths      []Path
	TotalValue int
}

func (bom BOM) String() string {
	str := ""
	for enditem := range bom {
		str += enditem + "\n"
		for _, child := range bom[enditem].Children {
			str += child.string(2)
		}
	}
	return str
}

func (item *Child) string(level int) string {
	str := strings.Repeat("\t", level-1) +
		item.Element.ID +
		"[" + strconv.Itoa(item.Quantity) + "]" +
		" [Level: " + strconv.Itoa(level) + "]" + "\n"
	if item.Element.Children != nil {
		for _, component := range item.Element.Children {
			str += component.string(level + 1)
		}
	}
	return str
}

func (p *Path) String() string {
	str := p.Root.ID
	for _, item := range p.Items {
		str += " - " + item.Element.ID
	}
	str += "\tPath Quantity: " + strconv.Itoa(p.Value)
	return str
}

func (pl *PathList) String() string {
	str := ""
	for _, path := range pl.Paths {
		str += path.String() + "\n"
	}
	str += "Quantity: " + strconv.Itoa(pl.TotalValue)
	return str
}
