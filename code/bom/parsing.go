package bom

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

// Parser reads the bom.txt files and returns a forest where each enditem constitutes the root of its own tree hierarchy
func Parser(filepath string) (BOM, error) {
	items := map[string]*Item{}
	endItems := map[string]*Item{}
	children := map[string]*Item{}
	file, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		row := strings.Split(strings.Trim(line, "|"), "|")
		if len(row) == 3 {
			fatherID := strings.TrimSpace(row[0])
			childID := strings.TrimSpace(row[1])
			quantity, err := strconv.Atoi(strings.TrimSpace(row[2]))
			if err != nil {
				return nil, err
			}
			father := Item{}
			child := Child{}
			if _, exists := items[childID]; !exists {
				childItem := Item{
					ID:       childID,
					Children: []*Child{},
				}
				items[childID] = &childItem
			}
			child = Child{
				Element:  items[childID],
				Quantity: quantity,
			}
			if _, exists := items[fatherID]; !exists {
				father = Item{
					ID:       fatherID,
					Children: []*Child{},
				}
				items[fatherID] = &father
			}
			if !contains(items[fatherID].Children, child) {
				items[fatherID].Children = append(items[fatherID].Children, &child)
			}
			endItems[fatherID] = items[fatherID]
			children[childID] = items[childID]
		}
	}
	for child := range children {
		delete(endItems, child)
	}
	return endItems, nil
}

func contains(children []*Child, child Child) bool {
	for i := range children {
		if children[i].Element.ID == child.Element.ID {
			return true
		}
	}
	return false
}
