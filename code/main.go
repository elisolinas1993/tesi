package main

import (
	"fmt"
	"strings"

	"gitlab.com/elisolinas1993/tesi/bozza_tesi/codice/bom"
)

func main() {
	bomTree, err := bom.Parser("fixtures/bom_es2.txt")
	if err != nil {
		panic(err)
	}
	fmt.Print(bomTree.String())

	incomplete := []string{"Componente A", "Componente B", "Componente C"}
	pathList, err := bomTree.Lookup(incomplete)
	if err != nil {
		panic(err)
	}
	fmt.Println("Looking for: [", strings.Join(incomplete, " - "), "]")
	fmt.Println(pathList.String())
}
